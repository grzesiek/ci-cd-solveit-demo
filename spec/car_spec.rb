require_relative "../car"

RSpec.describe Car do
  it "is subaru" do
    model = Car.new.model

    expect(model).to eq "subaru impreza"
  end
end
